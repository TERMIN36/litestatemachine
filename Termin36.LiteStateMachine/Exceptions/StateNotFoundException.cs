﻿namespace Termin36.LiteStateMachine.Exceptions;

public class StateNotFoundException : Exception
{
    public StateNotFoundException()
    {
    }

    public StateNotFoundException(string message) : base(message)
    {
    }

    public StateNotFoundException(string message, Exception inner) : base(message, inner)
    {
    }
}