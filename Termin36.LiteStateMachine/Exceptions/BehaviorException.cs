﻿namespace Termin36.LiteStateMachine.Exceptions;

public class BehaviorException : Exception
{
    public BehaviorException()
    {
    }

    public BehaviorException(string message, GuardDescription guard) : base(message)
    {
        Data.Add("GuardDescription", guard);
    }

    public BehaviorException(string message, Exception inner) : base(message, inner)
    {
    }

    public GuardDescription? GetGuardDescription()
    {
        return (GuardDescription?) Data["GuardDescription"];
    }
}