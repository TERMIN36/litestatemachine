﻿namespace Termin36.LiteStateMachine;

public class TriggerWithParameter<TTrigger, TArg>
{
    public TriggerWithParameter(TTrigger trigger)
    {
        Trigger = trigger;
        TypeArg = typeof(TArg);
    }

    public TTrigger Trigger { get; init; }
    public Type TypeArg { get; init; }
}