﻿using Termin36.LiteStateMachine.Behaviors;

namespace Termin36.LiteStateMachine;

/// <summary>
///     Класс реализующий конфигурацию SM для конкретного состояния
/// </summary>
/// <typeparam name="TState"></typeparam>
/// <typeparam name="TTrigger"></typeparam>
public class Configuration<TState, TTrigger> where TState : struct, Enum
{
    /// <summary>
    ///     Конструктор конфигурации
    /// </summary>
    /// <param name="stateMachine">SM родитель</param>
    public Configuration(StateMachine<TState, TTrigger> stateMachine)
    {
        StateMachine = stateMachine;
    }

    private StateMachine<TState, TTrigger> StateMachine { get; set; }
    private List<Func<Task>> OnActivations { get; set; } = new();
    private List<Func<Transaction<TState, TTrigger>?, Task>> OnEntries { get; set; } = new();

    private List<(TTrigger trigger, Func<Transaction<TState, TTrigger>?, Task> func)> OnEntriesFrom { get; set; } =
        new();

    private List<Func<Task>> OnDeactivations { get; set; } = new();
    private List<Func<Transaction<TState, TTrigger>?, Task>> OnExits { get; set; } = new();

    private List<(TTrigger trigger, Func<Transaction<TState, TTrigger>?, Task> func)> OnExitsFrom { get; set; } = new();
    private List<Behavior<TTrigger>> Behaviors { get; set; } = new();

    #region OnAction

    /// <summary>
    ///     Реакция на событие, при возникновении будет вызыван обработчик
    /// </summary>
    /// <param name="trigger">Триггер на который необходимо реагировать</param>
    /// <param name="action">Событие которое будет вызвано при срабатывании триггера</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnAction<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TArg?, Task> action)
    {
        Behaviors.Add(new ActionBehavior<TTrigger, TArg>(trigger, action, null));
        return this;
    }

    /// <summary>
    ///     Реакция на событие, при возникновении будет вызыван обработчик. При получении события проверяется condition
    /// </summary>
    /// <param name="trigger">Триггер на который необходимо реагировать</param>
    /// <param name="action">Событие которое будет вызвано при срабатывании триггера</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат false, обработка не
    ///     производится
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnActionIf<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TArg?, Task> action, Func<TArg?, Task<(bool result, string? message)>> condition,
        string? description = null)
    {
        Behaviors.Add(
            new ActionBehavior<TTrigger, TArg>(trigger, action, new Guard<TArg>(condition, description)));
        return this;
    }

    /// <summary>
    ///     Реакция на событие, при возникновении будет вызыван обработчик. При получении события проверяется condition
    /// </summary>
    /// <param name="trigger">Триггер на который необходимо реагировать</param>
    /// <param name="action">Событие которое будет вызвано при срабатывании триггера</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат true, обработка не
    ///     производится
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnActionIfInverted<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TArg?, Task> action, Func<TArg?, Task<(bool result, string? message)>> condition,
        string? description = null)
    {
        Behaviors.Add(
            new ActionBehavior<TTrigger, TArg>(trigger, action, new Guard<TArg>(condition, description, true)));
        return this;
    }

    #endregion

    #region NotChange

    /// <summary>
    ///     Принятие триггера (события), при этом ничего не просходит в результате. Эквивалент игнорирования события
    /// </summary>
    /// <param name="trigger">Триггер который необходимо игнорировать</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> NotChangeState<TArg>(TriggerWithParameter<TTrigger, TArg> trigger)
    {
        Behaviors.Add(new NotChangeStateBehavior<TTrigger, TArg>(trigger, null));
        return this;
    }

    /// <summary>
    ///     Принятие триггера (события), при этом ничего не просходит в результате если condition вернет true. Эквивалент
    ///     игнорирования события с проверкой условия
    /// </summary>
    /// <param name="trigger">Триггер который необходимо игнорировать</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат false, игнорирования
    ///     не происходит
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> NotChangeStateIf<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TArg?, Task<(bool result, string? message)>> condition, string? description = null)
    {
        Behaviors.Add(
            new NotChangeStateBehavior<TTrigger, TArg>(trigger, new Guard<TArg>(condition, description)));
        return this;
    }

    /// <summary>
    ///     Принятие триггера (события), при этом ничего не просходит в результате если condition вернет true. Эквивалент
    ///     игнорирования события с проверкой условия
    /// </summary>
    /// <param name="trigger">Триггер который необходимо игнорировать</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат true, игнорирования
    ///     не происходит
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> NotChangeStateIfInverted<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TArg?, Task<(bool result, string? message)>> condition, string? description = null)
    {
        Behaviors.Add(
            new NotChangeStateBehavior<TTrigger, TArg>(trigger, new Guard<TArg>(condition, description, true)));
        return this;
    }

    #endregion

    protected internal async Task<(Behavior<TTrigger>? behavior, List<GuardDescription>? message)>
        GetBehavior<TArg>(
            TriggerWithParameter<TTrigger, TArg> trigger, TArg? arg)
    {
        var behaviors = Behaviors
            .Where(x => x.Trigger.Equals(trigger.Trigger))
            .ToList();

        #region TODO отрефачить

        List<GuardDescription> messages = new();
        var changeState = await GetBehaviorByType<ChangeStateBehavior<TState, TTrigger, TArg>>(trigger, arg,
            behaviors,
            x => messages.Add(x));
        if (changeState.result)
            return changeState.internalGetBehavior;

        var notChangeState = await GetBehaviorByType<NotChangeStateBehavior<TTrigger, TArg>>(trigger, arg,
            behaviors,
            x => messages.Add(x));
        if (notChangeState.result)
            return notChangeState.internalGetBehavior;

        var actionIf = await GetBehaviorByType<ActionBehavior<TTrigger, TArg>>(trigger, arg, behaviors,
            x => messages.Add(x));
        if (actionIf.result)
            return actionIf.internalGetBehavior;

        var changeStateDynamic = await GetBehaviorByType<ChangeStateDynamicBehavior<TState, TTrigger, TArg>>(
            trigger,
            arg, behaviors,
            x => messages.Add(x));
        if (changeStateDynamic.result)
            return changeStateDynamic.internalGetBehavior;

        if (!messages.Any())
            messages.Add(
                new GuardDescription(
                    $"Не найдено ни одного обработчика для триггера {trigger.Trigger} в состоянии {StateMachine.State} у {StateMachine.EntityName}"));

        #endregion

        return (null, messages);

        async Task<(bool result, (Behavior<TTrigger>? behavior, List<GuardDescription>? message) internalGetBehavior
                )>
            GetBehaviorByType<T>(TriggerWithParameter<TTrigger, TArg> triggerWithParameter, TArg? arg,
                List<Behavior<TTrigger>> list,
                Action<GuardDescription> action)
            where T : Behavior<TTrigger>, IBehaviorWithGuard<TArg>
        {
            var baseBehavior = await
                Behavior<TTrigger>.GetBaseBehavior<T, TState, TArg>(
                    StateMachine.EntityName,
                    trigger: triggerWithParameter,
                    state: StateMachine.State,
                    behaviors: list, arg: arg);
            if (baseBehavior.message is not null)
                action.Invoke(baseBehavior.message);
            if (baseBehavior.behavior is not null)
            {
                return (true, (
                    baseBehavior.behavior,
                    message: new List<GuardDescription> {baseBehavior.message}));
            }

            return (false, (null, new List<GuardDescription> {baseBehavior.message}));
        }
    }

    #region ChangeState

    /// <summary>
    ///     Принятие триггера (события), при этом производится переход в состояние указанное в параметре
    /// </summary>
    /// <param name="trigger">Триггер который необходимо обрабатывать</param>
    /// <param name="state">Целевое состояние</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> ChangeState<TArg>(TriggerWithParameter<TTrigger, TArg> trigger, TState state)
    {
        Behaviors.Add(new ChangeStateBehavior<TState, TTrigger, TArg>(trigger, state, null));
        return this;
    }

    /// <summary>
    ///     Принятие триггера (события), при этом производится переход в состояние указанное в параметре если выполняется
    ///     условие в condition
    /// </summary>
    /// <param name="trigger">Триггер который необходимо обрабатывать</param>
    /// <param name="state">Целевое состояние</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат true, переход не
    ///     осуществляется
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> ChangeStateIfInverted<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        TState state, Func<TArg?, Task<(bool result, string? message)>> condition, string? description = null)
    {
        Behaviors.Add(
            new ChangeStateBehavior<TState, TTrigger, TArg>(trigger, state,
                new Guard<TArg>(condition, description, true)));
        return this;
    }

    /// <summary>
    ///     Принятие триггера (события), при этом производится переход в состояние указанное в параметре если выполняется
    ///     условие в condition
    /// </summary>
    /// <param name="trigger">Триггер который необходимо обрабатывать</param>
    /// <param name="state">Целевое состояние</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат false, переход не
    ///     осуществляется
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> ChangeStateIf<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        TState state, Func<TArg?, Task<(bool result, string? message)>> condition, string? description = null)
    {
        Behaviors.Add(
            new ChangeStateBehavior<TState, TTrigger, TArg>(trigger, state, new Guard<TArg>(condition, description)));
        return this;
    }

    /// <summary>
    ///     Принятие триггера (события), при этом производится переход в состояние которое определяется выражением
    /// </summary>
    /// <param name="trigger">Триггер который необходимо обрабатывать</param>
    /// <param name="navigator">Выражение, которое определяет следующее состояние</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> ChangeStateDynamic<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TriggerWithParameter<TTrigger, TArg>, TArg, TState> navigator)
    {
        Behaviors.Add(new ChangeStateDynamicBehavior<TState, TTrigger, TArg>(trigger, navigator, null));
        return this;
    }

    /// <summary>
    ///     Принятие триггера (события), при этом производится переход в состояние которое определяется выражением если
    ///     выполняется условие в condition
    /// </summary>
    /// <param name="trigger">Триггер который необходимо обрабатывать</param>
    /// <param name="navigator">Выражение, которое определяет следующее состояние</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат false, переход не
    ///     осуществляется
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> ChangeStateDynamicIf<TArg>(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TriggerWithParameter<TTrigger, TArg>, TArg, TState> navigator,
        Func<TArg?, Task<(bool result, string? message)>> condition,
        string? description = null)
    {
        Behaviors.Add(new ChangeStateDynamicBehavior<TState, TTrigger, TArg>(trigger, navigator,
            new Guard<TArg>(condition, description)));
        return this;
    }

    /// <summary>
    ///     Принятие триггера (события), при этом производится переход в состояние которое определяется выражением если
    ///     выполняется условие в condition
    /// </summary>
    /// <param name="trigger">Триггер который необходимо обрабатывать</param>
    /// <param name="navigator">Выражение, которое определяет следующее состояние</param>
    /// <param name="condition">
    ///     Выражение, которое проверяется при срабатывании триггера, если результат true, переход не
    ///     осуществляется
    /// </param>
    /// <param name="description">Используется как подсказка в случаях, когда не найден обработкик способный принять событие</param>
    /// <typeparam name="TArg">Тип параметра триггера</typeparam>
    /// <returns></returns>
    public Configuration<TState, TTrigger> ChangeStateDynamicIfInverted<TArg>(
        TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TriggerWithParameter<TTrigger, TArg>, TArg, TState> navigator,
        Func<TArg?, Task<(bool result, string? message)>> condition,
        string? description = null)
    {
        Behaviors.Add(new ChangeStateDynamicBehavior<TState, TTrigger, TArg>(trigger, navigator,
            new Guard<TArg>(condition, description, true)));
        return this;
    }

    #endregion

    #region Activate

    /// <summary>
    ///     Обработка события активации состояния. Выполняется когда у SM вызывают метод
    ///     <see cref="StateMachine{TState,TTrigger}.Activate" />
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnActivate(Func<Task> action)
    {
        OnActivations.Add(action);
        return this;
    }

    /// <summary>
    ///     Метод для активации текущего состояния. В результате будет вызван обработчик <see cref="OnActivate" />
    /// </summary>
    public async Task Activate()
    {
        foreach (var action in OnActivations)
        {
            await action.Invoke();
        }
    }

    #endregion

    #region Deactivate

    /// <summary>
    ///     Обработка события деактивации состояния. Выполняется когда у SM вызывают метод
    ///     <see cref="StateMachine{TState,TTrigger}.Deactivate" />
    /// </summary>
    /// <param name="action"></param>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnDeactivate(Func<Task> action)
    {
        OnDeactivations.Add(action);
        return this;
    }

    /// <summary>
    ///     Метод для деактивации текущего состояния. В результате будет вызван обработчик <see cref="OnDeactivate" />
    /// </summary>
    public async Task Deactivate()
    {
        foreach (var action in OnDeactivations)
        {
            await action.Invoke();
        }
    }

    #endregion

    #region Entry

    /// <summary>
    ///     Обработчик который будет вызван в когда SM переключает состояния. Именно этот метод вызывается при входе в
    ///     состояние
    /// </summary>
    /// <param name="action">Транзакция перехода, в ней имеются сведения о переходе</param>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnEntry(Func<Transaction<TState, TTrigger>?, Task> action)
    {
        OnEntries.Add(action);
        return this;
    }

    /// <summary>
    ///     Обработчик который будет вызван в когда SM переключает состояния. Именно этот метод вызывается при входе в
    ///     состояние
    ///     Перед вызовом присутствует проверка, которая выполняется только при условии совпадения триггера
    /// </summary>
    /// <param name="trigger">Триггер, при котором обработчик будет выполнен</param>
    /// <param name="action">Транзакция перехода, в ней имеются сведения о переходе</param>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnEntryFrom(TTrigger trigger,
        Func<Transaction<TState, TTrigger>?, Task> action)
    {
        OnEntriesFrom.Add((trigger, action));
        return this;
    }

    internal async Task Entry<TState, TTrigger>(Transaction<TState, TTrigger>? transaction)
    {
        foreach (var func in OnEntries)
        {
            var action = (Func<Transaction<TState, TTrigger>?, Task>) func;
            await action.Invoke(transaction);
        }

        if (transaction is not null)
        {
            foreach (var func in OnEntriesFrom.Where(x => x.trigger!.Equals(transaction.Trigger)))
            {
                var action = (Func<Transaction<TState, TTrigger>?, Task>) func.func;
                await action.Invoke(transaction);
            }
        }
    }

    #endregion

    #region Exit

    /// <summary>
    ///     Обработчик который будет вызван в когда SM переключает состояния. Именно этот метод вызывается при выходе из
    ///     состояния
    /// </summary>
    /// <param name="action">Транзакция перехода, в ней имеются сведения о переходе</param>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnExit(Func<Transaction<TState, TTrigger>?, Task> action)
    {
        OnExits.Add(action);
        return this;
    }

    /// <summary>
    ///     Обработчик который будет вызван в когда SM переключает состояния. Именно этот метод вызывается при выходе из
    ///     состояния
    ///     Перед вызовом присутствует проверка, которая выполняется только при условии совпадения триггера
    /// </summary>
    /// <param name="action">Транзакция перехода, в ней имеются сведения о переходе</param>
    /// <returns></returns>
    public Configuration<TState, TTrigger> OnExitFrom(TTrigger trigger,
        Func<Transaction<TState, TTrigger>?, Task> action)
    {
        OnExitsFrom.Add((trigger, action));
        return this;
    }

    internal async Task Exit<TState, TTrigger>(Transaction<TState, TTrigger>? transaction)
    {
        foreach (var func in OnExits)
        {
            var action = (Func<Transaction<TState, TTrigger>?, Task>) func;
            await action.Invoke(transaction);
        }

        if (transaction is not null)
        {
            foreach (var func in OnExitsFrom.Where(x => x.trigger!.Equals(transaction.Trigger)))
            {
                var action = (Func<Transaction<TState, TTrigger>?, Task>) func.func;
                await action.Invoke(transaction);
            }
        }
    }

    #endregion
}