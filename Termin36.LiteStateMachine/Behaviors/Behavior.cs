﻿using System.Diagnostics.CodeAnalysis;

namespace Termin36.LiteStateMachine.Behaviors;

public abstract class Behavior<TTrigger>
{
    protected Behavior([NotNull] TTrigger trigger)
    {
        Trigger = trigger ?? throw new ArgumentNullException(nameof(trigger));
    }

    [NotNull] public TTrigger Trigger { get; protected set; }

    public static async Task<(Behavior<TTrigger>? behavior, GuardDescription? message)>
        GetBaseBehavior<T, TState, TArg>(
            string entityName,
            TriggerWithParameter<TTrigger, TArg> trigger,
            TState state,
            List<Behavior<TTrigger>> behaviors, TArg? arg)
        where T : Behavior<TTrigger>, IBehaviorWithGuard<TArg>
    {
        var behaviorOfType = behaviors
            .OfType<T>()
            .ToList();

        var behaviorWithoutGuard = behaviorOfType.Where(x => x.Guard is null).ToList();

        var countBehaviorWithoutGuard = behaviorWithoutGuard.Count;

        var behaviorWithGuard = behaviorOfType
            .Where(x => x.Guard is not null).ToList();

        var behaviorWithGuardResults = await behaviorWithGuard.ToAsyncEnumerable()
            .SelectAwait(async x =>
            {
                var res = await x.Guard!.Condition.Invoke(arg);
                if (x.Guard!.InvertedCondition)
                {
                    res = (!res.result, res.message);
                }

                return new
                {
                    behavior = x, guard = res
                };
            })
            .ToListAsync();

        var behaviorWithGuardSuccess = behaviorWithGuardResults
            .Where(x => x.guard.result).ToList();

        var countBehaviorWithGuard = behaviorWithGuardSuccess.Count;

        if (countBehaviorWithGuard + countBehaviorWithoutGuard > 1)
            return (behavior: null,
                message: new GuardDescription(
                    $"У триггера {trigger.Trigger} обнаружено более одного срабатывающего обработчика типа {nameof(T)} в состоянии {state} у {entityName}",
                    typeof(T)));

        if (countBehaviorWithGuard + countBehaviorWithoutGuard == 0)
        {
            var messages = behaviorWithGuardResults.Where(x => x.guard is {result: false, message: not null})
                .Select(x => x.guard.message)
                .Union(
                    behaviorWithGuard.Where(x => x.Guard?.Description is not null)
                        .Select(x => x.Guard!.Description!))
                .ToArray();
            if (behaviorOfType.Any())
            {
                var fullMessage =
                    $"Для триггера {trigger.Trigger} в состоянии {state} у {entityName} не найден ни один обработчик типа {typeof(T).Name}, условия которого выполнены.";
                if (messages.Any())
                    fullMessage += $" Варианты причин: {string.Join(";", messages)}";
                return (behavior: null, message: new GuardDescription(fullMessage, typeof(T), messages));
            }

            return (behavior: null, message: null);
        }

        return (behavior: behaviorWithoutGuard.Union(behaviorWithGuardSuccess.Select(x => x.behavior)).FirstOrDefault(),
            message: null);
    }
}