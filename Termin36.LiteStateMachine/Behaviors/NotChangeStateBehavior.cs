﻿using Termin36.LiteStateMachine.Behaviors;

namespace Termin36.LiteStateMachine;

public class NotChangeStateBehavior<TTrigger, TArg> : Behavior<TTrigger>, IBehaviorWithGuard<TArg>
{
    public NotChangeStateBehavior(TriggerWithParameter<TTrigger, TArg> trigger, Guard<TArg>? guard) : this(trigger)
    {
        Guard = guard;
    }

    public NotChangeStateBehavior(TriggerWithParameter<TTrigger, TArg> trigger) : base(trigger.Trigger)
    {
    }

    public Guard<TArg>? Guard { get; set; }
}