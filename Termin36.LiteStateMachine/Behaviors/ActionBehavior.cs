﻿using Termin36.LiteStateMachine.Behaviors;

namespace Termin36.LiteStateMachine;

public class ActionBehavior<TTrigger, TArg> : Behavior<TTrigger>, IBehaviorWithGuard<TArg>
{
    public ActionBehavior(TriggerWithParameter<TTrigger, TArg> trigger, Func<TArg?, Task> action, Guard<TArg>? guard) :
        this(trigger, action)
    {
        Guard = guard;
    }

    public ActionBehavior(TriggerWithParameter<TTrigger, TArg> trigger, Func<TArg?, Task> action) : base(
        trigger.Trigger)
    {
        Action = action;
    }

    public Func<TArg?, Task> Action { get; set; }

    public Guard<TArg>? Guard { get; set; }
}