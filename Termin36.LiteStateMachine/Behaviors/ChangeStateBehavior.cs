﻿using Termin36.LiteStateMachine.Behaviors;

namespace Termin36.LiteStateMachine;

public class ChangeStateBehavior<TState, TTrigger, TArg> : Behavior<TTrigger>, IBehaviorWithGuard<TArg>
{
    public ChangeStateBehavior(TriggerWithParameter<TTrigger, TArg> trigger, TState destinationState,
        Guard<TArg>? guard) : this(trigger,
        destinationState)
    {
        Guard = guard;
    }

    public ChangeStateBehavior(TriggerWithParameter<TTrigger, TArg> trigger,
        TState destinationState) : base(trigger.Trigger)
    {
        Destination = destinationState;
    }

    public TState Destination { get; private set; }

    public Guard<TArg>? Guard { get; set; }
}