﻿using Termin36.LiteStateMachine.Behaviors;

namespace Termin36.LiteStateMachine;

public class ChangeStateDynamicBehavior<TState, TTrigger, TArg> : Behavior<TTrigger>, IBehaviorWithGuard<TArg>
{
    public ChangeStateDynamicBehavior(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TriggerWithParameter<TTrigger, TArg>, TArg, TState> navigator,
        Guard<TArg>? guard) : this(trigger, navigator)
    {
        Guard = guard;
    }

    public ChangeStateDynamicBehavior(TriggerWithParameter<TTrigger, TArg> trigger,
        Func<TriggerWithParameter<TTrigger, TArg>, TArg?, TState> navigator) : base(trigger.Trigger)
    {
        Navigator = navigator;
    }

    public Func<TriggerWithParameter<TTrigger, TArg>, TArg?, TState> Navigator { get; set; }
    public Guard<TArg>? Guard { get; set; }
}