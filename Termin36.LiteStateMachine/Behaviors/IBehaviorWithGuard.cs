﻿namespace Termin36.LiteStateMachine;

public interface IBehaviorWithGuard<TArg>
{
    public Guard<TArg>? Guard { get; set; }
}