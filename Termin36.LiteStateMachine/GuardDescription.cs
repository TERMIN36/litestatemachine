﻿namespace Termin36.LiteStateMachine;

public class GuardDescription
{
    public GuardDescription(string fullMessage, Type? typeBehavior = null, string[]? guard = null)
    {
        FullMessage = fullMessage;
        TypeBehavior = typeBehavior;
        Guards = guard;
    }

    public string FullMessage { get; set; }
    public string[]? Guards { get; set; }

    public Type? TypeBehavior { get; set; }
}