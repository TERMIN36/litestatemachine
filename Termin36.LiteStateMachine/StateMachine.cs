﻿using Termin36.LiteStateMachine.Exceptions;

namespace Termin36.LiteStateMachine;

/// <summary>
///     Машина состояний (далее SM). После создания необходимо настроить конфигурации методом <see cref="Configuration" />
/// </summary>
/// <typeparam name="TState"></typeparam>
/// <typeparam name="TTrigger"></typeparam>
public class StateMachine<TState, TTrigger> where TState : struct, Enum
{
    #region Delegates

    /// <summary>
    ///     Обработчик RealTime переходов между состояниями
    /// </summary>
    public delegate void OnTransitionHandler(Transaction<TState, TTrigger> transaction);

    #endregion

    private readonly Func<TState, TTrigger, object?, Type?, List<GuardDescription>?, Exception>? _onFailHandler;

    private readonly Action<TState>? _stateUpdateHandle;

    private readonly List<Transaction<TState, TTrigger>> _transactions;

    /// <summary>
    ///     Конструктор для SM
    /// </summary>
    /// <param name="state">Начальное состояние с которого начнется работа</param>
    /// <param name="entityName">Имя машины состояний, нужно для вывода ошибок</param>
    /// <param name="onFailHandler">Обработчик для переопределения ошибок на свои собственные</param>
    /// <param name="stateUpdateHandle">
    ///     Обработчик, который, будет вызываться когда статус SM будет меняться.
    ///     Обычно используется для обновления статуса, который хранится за переделами SM
    /// </param>
    public StateMachine(TState state, string entityName, Func<TState, TTrigger, object?, Type?,
        List<GuardDescription>?, Exception>? onFailHandler = null, Action<TState>? stateUpdateHandle = null)
    {
        EntityName = entityName;
        State = state;
        _onFailHandler = onFailHandler;
        _transactions ??= new List<Transaction<TState, TTrigger>>();
        _stateUpdateHandle = stateUpdateHandle;
    }

    /// <summary>
    ///     Список транзакций с переходами
    /// </summary>
    public IReadOnlyCollection<Transaction<TState, TTrigger>> Transactions => _transactions;

    /// <summary>
    ///     Состояние в котором находится SM
    /// </summary>
    public TState State { get; private set; }

    /// <summary>
    ///     Имя для экземпляра SM
    /// </summary>
    public string EntityName { get; private set; }

    private Dictionary<TState, Configuration<TState, TTrigger>> Configurations { get; set; } = new();

    /// <summary>
    ///     Событие перехода между состояниями
    /// </summary>
    public event OnTransitionHandler? OnTransition;

    /// <summary>
    ///     Метод для конфигурирования SM
    /// </summary>
    /// <param name="state">Состояние которое необходимо сконфигурировать</param>
    /// <returns>
    ///     Конфигурация для последовательного вызова таких методов как
    ///     <see cref="Configuration{TState,TTrigger}.OnAction{TArg}" />,
    ///     <see cref="Configuration{TState,TTrigger}.ChangeState{TArg}" /> и т.д."/>
    /// </returns>
    public Configuration<TState, TTrigger> Configuration(TState state)
    {
        if (Configurations.TryGetValue(state, out var configuration)) return configuration;

        configuration = new Configuration<TState, TTrigger>(this);
        Configurations.Add(state, configuration);

        return configuration;
    }

    /// <summary>
    ///     Запуск валидации конфигурации SM
    /// </summary>
    /// <returns>Ответ true, false. Стоит ожидать, в том числе исключений</returns>
    public bool ValidateStateMachine()
    {
        foreach (var state in Enum.GetValues<TState>())
        {
            GetConfiguration(state);
        }

        return true;
    }

    /// <summary>
    ///     Проверить, возможно ли вызвать триггер, и заранее получить ошибки. В результате работы не совершаются переходы
    ///     состояний и не вызываются Actions
    /// </summary>
    /// <param name="trigger">Триггер (событие)</param>
    /// <param name="arg">Пользовательский параметр (будет доставлен в некоторые события)</param>
    /// <typeparam name="TArg">Тип пользовательского параметра</typeparam>
    /// <returns>в result ответ в виде true, false. В message коллекция ошибок, возникших в результате проверки</returns>
    public async Task<(bool result, List<GuardDescription>? message)> CanDo<TArg>(
        TriggerWithParameter<TTrigger, TArg> trigger, TArg? arg)
    {
        return await CanFire(trigger, arg);
    }

    /// <summary>
    ///     Проверить, возможно ли вызвать триггер, и заранее получить ошибки. В результате работы не совершаются переходы
    ///     состояний и не вызываются Actions
    /// </summary>
    /// <param name="trigger">Триггер (событие)</param>
    /// <returns>в result ответ в виде true, false. В message коллекция ошибок, возникших в результате проверки</returns>
    public async Task<(bool result, List<GuardDescription>? message)> CanDo(
        TriggerWithParameter<TTrigger, object?> trigger)
    {
        return await CanFire(trigger, null);
    }

    /// <summary>
    ///     Проверить, возможно ли вызвать триггер, и заранее получить ошибки. В результате работы не совершаются переходы
    ///     состояний и не вызываются Actions
    /// </summary>
    /// <param name="trigger">Триггер (событие)</param>
    /// <param name="arg">Пользовательский параметр (будет доставлен в некоторые события)</param>
    /// <typeparam name="TArg">Тип пользовательского параметра</typeparam>
    /// <returns>в result ответ в виде true, false. В message коллекция ошибок, возникших в результате проверки</returns>
    [Obsolete("Производится переход на метод CanDo")]
    public async Task<(bool result, List<GuardDescription>? message)> CanFire<TArg>(
        TriggerWithParameter<TTrigger, TArg> trigger, TArg? arg)
    {
        var configuration = GetConfiguration();
        var behavior = await configuration.GetBehavior(trigger, arg);
        if (behavior.behavior is not null)
        {
            return (true, null);
        }

        if (_onFailHandler is not null)
        {
            try
            {
                _onFailHandler.Invoke(State, trigger.Trigger, arg, trigger.TypeArg, behavior.message);
            }
            catch (Exception e)
            {
                return (false, new List<GuardDescription> {new(e.Message)});
            }
        }

        return (false, behavior.message);
    }

    /// <summary>
    ///     Вызвать триггер (событие). В результате работы совершаются переходы состояний и вызываются Actions
    /// </summary>
    /// <param name="trigger">Триггер (событие)</param>
    /// <param name="arg">Пользовательский параметр (будет доставлен в некоторые события)</param>
    /// <typeparam name="TArg">Тип пользовательского параметра</typeparam>
    /// <exception cref="NotImplementedException"></exception>
    /// <exception cref="BehaviorException">
    ///     Класс исключений срабатывающих в результате неожиданного поведения настроенной
    ///     конфигурации
    /// </exception>
    public async Task Do<TArg>(TriggerWithParameter<TTrigger, TArg> trigger, TArg? arg)
    {
        await Fire(trigger, arg);
    }

    /// <summary>
    ///     Вызвать триггер (событие). В результате работы совершаются переходы состояний и вызываются Actions
    /// </summary>
    /// <param name="trigger">Триггер (событие)</param>
    /// <exception cref="NotImplementedException"></exception>
    /// <exception cref="BehaviorException">
    ///     Класс исключений срабатывающих в результате неожиданного поведения настроенной
    ///     конфигурации
    /// </exception>
    public async Task Do(TriggerWithParameter<TTrigger, object?> trigger)
    {
        await Fire(trigger, null);
    }

    /// <summary>
    ///     Вызвать триггер (событие). В результате работы совершаются переходы состояний и вызываются Actions
    /// </summary>
    /// <param name="trigger">Триггер (событие)</param>
    /// <param name="arg">Пользовательский параметр (будет доставлен в некоторые события)</param>
    /// <typeparam name="TArg">Тип пользовательского параметра</typeparam>
    /// <exception cref="NotImplementedException"></exception>
    /// <exception cref="BehaviorException">
    ///     Класс исключений срабатывающих в результате неожиданного поведения настроенной
    ///     конфигурации
    /// </exception>
    [Obsolete("Производится переход на метод Do")]
    public async Task Fire<TArg>(TriggerWithParameter<TTrigger, TArg> trigger, TArg? arg)
    {
        var configuration = GetConfiguration();
        var behavior = await configuration.GetBehavior(trigger, arg);
        if (behavior.behavior != null)
        {
            switch (behavior.behavior)
            {
                case ChangeStateBehavior<TState, TTrigger, TArg> changeStateBehavior:
                    await InternalTransition(configuration, new Transaction<TState, TTrigger>(
                        trigger: trigger.Trigger,
                        arg: arg,
                        argType: typeof(TArg),
                        stateSource: State,
                        stateDestination: changeStateBehavior.Destination));
                    break;
                case NotChangeStateBehavior<TTrigger, TArg>:
                    break;
                case ActionBehavior<TTrigger, TArg> actionBehavior:
                    await actionBehavior.Action.Invoke(arg);
                    break;
                case ChangeStateDynamicBehavior<TState, TTrigger, TArg> changeStateDynamicBehavior:
                    var destination = changeStateDynamicBehavior.Navigator.Invoke(trigger, arg);
                    await InternalTransition(configuration, new Transaction<TState, TTrigger>(
                        trigger: trigger.Trigger,
                        arg: arg,
                        argType: typeof(TArg),
                        stateSource: State,
                        stateDestination: destination));
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
        else
        {
            if (_onFailHandler is not null)
            {
                throw _onFailHandler.Invoke(State, trigger.Trigger, arg, trigger.TypeArg, behavior.message);
            }

            throw new BehaviorException(behavior.message!.First().FullMessage, behavior.message!.First());
        }
    }

    private async Task InternalTransition(Configuration<TState, TTrigger> configuration,
        Transaction<TState, TTrigger> transaction)
    {
        await configuration.Exit(transaction);
        State = transaction.StateDestination;
        _stateUpdateHandle?.Invoke(State);
        await GetConfiguration().Entry(transaction);
        _transactions.Add(transaction);
        OnTransition?.Invoke(transaction);
    }

    /// <summary>
    ///     Произвести активацию вручную текущего состояния. В результате будет вызвано событие OnActivate
    /// </summary>
    public async Task Activate()
    {
        await GetConfiguration().Activate();
    }

    /// <summary>
    ///     Произвести деактивацию вручную текущего состояния. В результате будет вызвано событие OnDeactivate
    /// </summary>
    public async Task Deactivate()
    {
        await GetConfiguration().Deactivate();
    }

    private Configuration<TState, TTrigger> GetConfiguration(TState? state = null)
    {
        if (!TryGetConfiguration(state ?? State, out var configuration))
            throw new StateNotFoundException(
                $"Состояние {state} не было сконфигурировано в {EntityName}");
        return configuration!;
    }

    private bool TryGetConfiguration(TState? state, out Configuration<TState, TTrigger>? configuration)
    {
        state ??= State;

        var tryGetResult = Configurations.TryGetValue((TState) state, out var value);
        if (tryGetResult)
        {
            configuration = value;
            return true;
        }

        configuration = null;
        return false;
    }
}