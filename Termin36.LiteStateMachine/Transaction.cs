﻿namespace Termin36.LiteStateMachine;

/// <summary>
///     Класс транзакции, служит для фиксирования перехода между состояниями SM
/// </summary>
/// <typeparam name="TState"></typeparam>
/// <typeparam name="TTrigger"></typeparam>
public class Transaction<TState, TTrigger>
{
    public Transaction(TTrigger trigger, object? arg, Type argType, TState stateSource, TState stateDestination)
    {
        Trigger = trigger;
        Arg = arg;
        ArgType = argType;
        StateSource = stateSource;
        StateDestination = stateDestination;
    }

    /// <summary>
    ///     Триггер, по срабатыванию которого произошел переход
    /// </summary>
    public TTrigger Trigger { get; init; }

    /// <summary>
    ///     Параметр, который был передан при срабатывании триггера
    /// </summary>
    public object? Arg { get; init; }

    /// <summary>
    ///     Информация о типе параметра
    /// </summary>
    public Type ArgType { get; init; }

    /// <summary>
    ///     Исходное состояние
    /// </summary>
    public TState StateSource { get; init; }

    /// <summary>
    ///     Целевое состояние
    /// </summary>
    public TState StateDestination { get; init; }
}