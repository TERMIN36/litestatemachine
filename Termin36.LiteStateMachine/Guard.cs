﻿namespace Termin36.LiteStateMachine;

public class Guard<TArg>
{
    public Guard(Func<TArg?, Task<(bool result, string? message)>> condition, string? description,
        bool? inverted = null)
    {
        Condition = condition;
        Description = description;
        InvertedCondition = inverted ?? false;
    }

    /// <summary>
    ///     Выражение для проверки. В случае успеха должно вернуть true, в случае провала, false и сообщение, если оно имеется
    /// </summary>
    public Func<TArg?, Task<(bool result, string? message)>> Condition { get; private set; }

    public string? Description { get; private set; }
    public bool InvertedCondition { get; init; }
}