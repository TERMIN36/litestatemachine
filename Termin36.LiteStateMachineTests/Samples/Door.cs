﻿using Termin36.LiteStateMachine;

namespace Termin36.LiteStateMachineTests.Samples;

[TestFixture]
public class Door
{
    public enum State
    {
        DoorClosedAndLocked,
        Unlocking,
        Locking,
        DoorClosedAndUnlocked,
        DoorOpen
    }

    public enum Trigger
    {
        EnterPinCode,
        OpenDoor,
        ManWalk,
        CloseDoor,
        LockEvent,
        UnlockEvent,
        LockDoor
    }

    public TriggerWithParameter<Trigger, string> EnterPinCode = new(Trigger.EnterPinCode);
    public TriggerWithParameter<Trigger, object> OpenDoor = new(Trigger.OpenDoor);
    public TriggerWithParameter<Trigger, string> ManWalk = new(Trigger.ManWalk);
    public TriggerWithParameter<Trigger, object> CloseDoor = new(Trigger.CloseDoor);
    public TriggerWithParameter<Trigger, object> LockEvent = new(Trigger.LockEvent);
    public TriggerWithParameter<Trigger, object> UnlockEvent = new(Trigger.UnlockEvent);
    public TriggerWithParameter<Trigger, object> LockDoor = new(Trigger.LockDoor);


    public StateMachine<State, Trigger> CreateSM(State state)
    {
        return ConfigSM(new StateMachine<State, Trigger>(state, nameof(Door)));
    }

    [Test]
    public async Task Good()
    {
        var sm = CreateSM(State.DoorClosedAndLocked);

        await sm.Do(EnterPinCode, "1234");
        await sm.Do(UnlockEvent, null);
        await sm.Do(OpenDoor, null);
        await sm.Do(ManWalk, "Петр");
        await sm.Do(CloseDoor, null);
        await sm.Do(LockDoor, null);
        await sm.Do(LockEvent, null);
    }

    public StateMachine<State, Trigger> ConfigSM(StateMachine<State, Trigger> sm)
    {
        sm.Configuration(State.DoorClosedAndLocked)
            .OnEntry(async _ => Console.WriteLine("Дверь заблокирована"))
            .ChangeStateIf(EnterPinCode, State.Unlocking, async pin => (pin.Equals("1234"), null), "Неверный пин код");

        sm.Configuration(State.Unlocking)
            .OnEntry(async _ => Console.WriteLine("Замок разблокируется"))
            .ChangeState(UnlockEvent, State.DoorClosedAndUnlocked)
            .OnExit(async _ => Console.WriteLine("Замок разблокировался"));

        sm.Configuration(State.DoorClosedAndUnlocked)
            .OnEntry(async _ => Console.WriteLine("Дверь закрыта и разблокирована"))
            .ChangeState(OpenDoor, State.DoorOpen)
            .ChangeState(LockDoor, State.Locking);

        sm.Configuration(State.DoorOpen)
            .OnEntry(async _ => Console.WriteLine("Дверь открыта"))
            .OnAction(ManWalk, async name => Console.WriteLine("В дверь прошел {0}", name))
            .ChangeState(CloseDoor, State.DoorClosedAndUnlocked);

        sm.Configuration(State.Locking)
            .OnEntry(async _ => Console.WriteLine("Замок блокируется"))
            .ChangeState(LockEvent, State.DoorClosedAndLocked)
            .OnExit(async _ => Console.WriteLine("Замок заблокировался"));
        return sm;
    }
}