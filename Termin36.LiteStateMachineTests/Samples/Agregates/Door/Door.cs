﻿using Termin36.LiteStateMachine;

namespace Termin36.LiteStateMachineTests.Samples.Agregates.Door;

public class Door : IDoor
{
    #region ctor

    public Door(State state, DoorLock.State stateLock, string pinCode)
    {
        CurrentState = state;
        Замок = new DoorLock(stateLock, pinCode);
        _stateMachine =
            new StateMachine<State, Trigger>(CurrentState, "Door", stateUpdateHandle: state1 => CurrentState = state1);

        Config().Wait();
    }

    #endregion

    #region ConfigureStateMachine

    public async Task Config()
    {
        _stateMachine.Configuration(State.ДверьЗакрыта)
            .OnActionIf(ВводимПинКод, Замок.Разблокировать, Замок.ПроверитьВозможностьРазблокировки)
            .OnAction(СобытиеЗамокРазблокирован, Замок.СобытиеРазблокировки)
            .ChangeStateIf(ОткрываемДверь, State.ДверьОткрыта, Замок.ПроверитьОткрытЛиЗамок);

        // тупо для удобства, потому что могу конфигурацию делить на секции), не нравится если, можно строку убрать вообще
        _stateMachine.Configuration(State.ДверьЗакрыта)
            .OnAction(БлокируемЗамок, Замок.Заблокировать)
            .OnAction(СобытиеЗамокЗаблокирован, Замок.СобытиеБлокировки);

        _stateMachine.Configuration(State.ДверьОткрыта)
            .ChangeStateIf(ЗакрываемДверь, State.ДверьЗакрыта, Замок.ПроверитьОткрытЛиЗамок);

        _stateMachine.ValidateStateMachine();
    }

    #endregion

    #region props

    protected State CurrentState { get; private protected set; }
    protected DoorLock Замок { get; set; }

    protected StateMachine<State, Trigger> _stateMachine;

    #endregion

    #region State and Triggers

    public enum State
    {
        ДверьОткрыта,
        ДверьЗакрыта
    }

    public enum Trigger
    {
        ОткрываемДверь,
        ЗакрываемДверь,
        ВводимПинКод,
        БлокируемЗамок,
        СобытиеЗамокЗаблокирован,
        СобытиеЗамокРазблокирован
    }


    private readonly TriggerWithParameter<Trigger, object?> ОткрываемДверь = new(Trigger.ОткрываемДверь);
    private readonly TriggerWithParameter<Trigger, object?> ЗакрываемДверь = new(Trigger.ЗакрываемДверь);
    private readonly TriggerWithParameter<Trigger, string> ВводимПинКод = new(Trigger.ВводимПинКод);
    private readonly TriggerWithParameter<Trigger, object?> БлокируемЗамок = new(Trigger.БлокируемЗамок);

    private readonly TriggerWithParameter<Trigger, object?> СобытиеЗамокЗаблокирован =
        new(Trigger.СобытиеЗамокЗаблокирован);

    private readonly TriggerWithParameter<Trigger, object?> СобытиеЗамокРазблокирован =
        new(Trigger.СобытиеЗамокРазблокирован);

    #endregion

    #region PublicMethods

    public async Task ОткрытьДверь()
    {
        _stateMachine.Do(ОткрываемДверь, null).Wait();
    }

    public async Task Разблокировать(string pinCode)
    {
        _stateMachine.Do(ВводимПинКод, pinCode).Wait();
    }

    public async Task ЗакрытьДверь()
    {
        _stateMachine.Do(ЗакрываемДверь, null).Wait();
    }

    public async Task ЗаблокироватьЗамок()
    {
        _stateMachine.Do(БлокируемЗамок, null).Wait();
    }

    public async Task СобытиеБлокировкиЗамка()
    {
        _stateMachine.Do(СобытиеЗамокЗаблокирован, null).Wait();
    }

    public async Task СобытиеРазблокировки()
    {
        _stateMachine.Do(СобытиеЗамокРазблокирован, null).Wait();
    }

    #endregion
}