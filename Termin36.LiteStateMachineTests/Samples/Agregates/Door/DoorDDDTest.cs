﻿using FluentAssertions;
using Termin36.LiteStateMachine.Exceptions;
using StateDoor = Termin36.LiteStateMachineTests.Samples.Agregates.Door.Door.State;
using TriggerDoor = Termin36.LiteStateMachineTests.Samples.Agregates.Door.Door.Trigger;
using StateLock = Termin36.LiteStateMachineTests.Samples.Agregates.Door.DoorLock.State;
using TriggerLock = Termin36.LiteStateMachineTests.Samples.Agregates.Door.DoorLock.Trigger;

namespace Termin36.LiteStateMachineTests.Samples.Agregates.Door;

[TestFixture]
public class DoorDDDTest
{
    [SetUp]
    public void SetUp()
    {
        door = new DoorStub(StateDoor.ДверьЗакрыта, StateLock.Закрыт, "1234");
    }

    public static DoorStub door { get; set; }
    private readonly string DoorName = "Door";
    private readonly string LockName = "DoorLock";

    [Test]
    public async Task Good()
    {
        await door.Разблокировать("1234");
        await door.СобытиеРазблокировки();
        await door.ОткрытьДверь();
        await door.ЗакрытьДверь();
        await door.ЗаблокироватьЗамок();
        await door.СобытиеБлокировкиЗамка();
    }

    [Test]
    public async Task Double_Разблокировать()
    {
        await door.Разблокировать("1234");
        var act = () => door.Разблокировать("1234");
        var res = await act.Should().ThrowAsync<BehaviorException>();
        res.And.GetGuardDescription()!.FullMessage.Should().Be(
            $"Для триггера ВводимПинКод в состоянии {StateDoor.ДверьЗакрыта} у Door не найден ни один обработчик типа ActionBehavior`2, условия которого выполнены.");
    }

    [Test]
    public async Task Double_СобытиеРазблокировки()
    {
        await door.Разблокировать("1234");
        await door.СобытиеРазблокировки();
        var act = () => door.СобытиеРазблокировки();
        var res = await act.Should().ThrowAsync<BehaviorException>();
        res.And.GetGuardDescription()!.FullMessage.Should().Be(CompileMessageNotFoundHandler(
            TriggerLock.СобытиеДляОткрытия.ToString(),
            StateLock.Открыт.ToString(), LockName));
    }

    [Test]
    public async Task Double_ОткрытьДверь()
    {
        await door.Разблокировать("1234");
        await door.СобытиеРазблокировки();
        await door.ОткрытьДверь();
        var act = () => door.ОткрытьДверь();
        var res = await act.Should().ThrowAsync<BehaviorException>();
        res.And.GetGuardDescription()!.FullMessage.Should().Be(CompileMessageNotFoundHandler(
            TriggerDoor.ОткрываемДверь.ToString(),
            StateDoor.ДверьОткрыта.ToString(), DoorName));
    }

    [Test]
    public async Task Double_ЗакрытьДверь()
    {
        door.SetState(StateDoor.ДверьОткрыта, new DoorLock(StateLock.Открыт, "1234"));
        await door.ЗакрытьДверь();
        var act = () => door.ЗакрытьДверь();
        var res = await act.Should().ThrowAsync<BehaviorException>();
        res.And.GetGuardDescription()!.FullMessage.Should().Be(CompileMessageNotFoundHandler(
            TriggerDoor.ЗакрываемДверь.ToString(),
            StateDoor.ДверьЗакрыта.ToString(), DoorName));
    }


    [Test]
    public async Task Double_ЗаблокироватьЗамок()
    {
        await door.Разблокировать("1234");
        await door.СобытиеРазблокировки();
        await door.ОткрытьДверь();
        await door.ЗакрытьДверь();
        await door.ЗаблокироватьЗамок();
        var act = () => door.ЗаблокироватьЗамок();
        var res = await act.Should().ThrowAsync<BehaviorException>();
        res.And.GetGuardDescription()!.FullMessage.Should().Be(CompileMessageNotFoundHandler(
            TriggerLock.Закрываем.ToString(),
            StateLock.Закрывается.ToString(), LockName));
    }

    [Test]
    public async Task Double_СобытиеБлокировкиЗамка()
    {
        await door.Разблокировать("1234");
        await door.СобытиеРазблокировки();
        await door.ОткрытьДверь();
        await door.ЗакрытьДверь();
        await door.ЗаблокироватьЗамок();
        await door.СобытиеБлокировкиЗамка();
        var act = () => door.СобытиеБлокировкиЗамка();
        var res = await act.Should().ThrowAsync<BehaviorException>();
        res.And.GetGuardDescription()!.FullMessage.Should().Be(
            CompileMessageNotFoundHandler(
                TriggerLock.СобытиеДляЗакрытия.ToString(),
                StateLock.Закрыт.ToString(), LockName));
    }

    [Test]
    [TestCaseSource(nameof(GetValues))]
    public async Task КомплексныйТест(
        (StateDoor stateDoor, StateLock stateLock, Func<IDoor, Task> act, bool result) value)
    {
        door = new DoorStub(value.stateDoor, value.stateLock, "1234");
        var act = async () => await value.act.Invoke(door);
        if (value.result)
        {
            await act.Should().NotThrowAsync<BehaviorException>();
        }
        else
        {
            await act.Should().ThrowAsync<BehaviorException>();
        }
    }

    public static IEnumerable<(StateDoor stateDoor, StateLock stateLock, Func<IDoor, Task> act, bool result)>
        GetValues()
    {
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрыт, door1 => door1.Разблокировать("1234"), true);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрыт, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрыт, door1 => door1.ОткрытьДверь(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрыт, door1 => door1.ЗакрытьДверь(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрыт, door1 => door1.СобытиеРазблокировки(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрыт, door1 => door1.СобытиеБлокировкиЗамка(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрыт, door1 => door1.ЗаблокироватьЗамок(), false);

        yield return (StateDoor.ДверьЗакрыта, StateLock.Открыт, door1 => door1.Разблокировать("1234"), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открыт, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открыт, door1 => door1.ОткрытьДверь(), true);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открыт, door1 => door1.ЗакрытьДверь(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открыт, door1 => door1.СобытиеРазблокировки(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открыт, door1 => door1.СобытиеБлокировкиЗамка(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открыт, door1 => door1.ЗаблокироватьЗамок(), true);

        yield return (StateDoor.ДверьЗакрыта, StateLock.Открывается, door1 => door1.Разблокировать("1234"), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открывается, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открывается, door1 => door1.ОткрытьДверь(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открывается, door1 => door1.ЗакрытьДверь(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открывается, door1 => door1.СобытиеРазблокировки(), true);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открывается, door1 => door1.СобытиеБлокировкиЗамка(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Открывается, door1 => door1.ЗаблокироватьЗамок(), false);

        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрывается, door1 => door1.Разблокировать("1234"), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрывается, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрывается, door1 => door1.ОткрытьДверь(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрывается, door1 => door1.ЗакрытьДверь(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрывается, door1 => door1.СобытиеРазблокировки(), false);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрывается, door1 => door1.СобытиеБлокировкиЗамка(), true);
        yield return (StateDoor.ДверьЗакрыта, StateLock.Закрывается, door1 => door1.ЗаблокироватьЗамок(), false);

        yield return (StateDoor.ДверьОткрыта, StateLock.Открыт, door1 => door1.Разблокировать("1234"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открыт, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открыт, door1 => door1.ОткрытьДверь(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открыт, door1 => door1.ЗакрытьДверь(), true);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открыт, door1 => door1.СобытиеРазблокировки(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открыт, door1 => door1.СобытиеБлокировкиЗамка(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открыт, door1 => door1.ЗаблокироватьЗамок(), false);

        yield return (StateDoor.ДверьОткрыта, StateLock.Открывается, door1 => door1.Разблокировать("1234"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открывается, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открывается, door1 => door1.ОткрытьДверь(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открывается, door1 => door1.ЗакрытьДверь(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открывается, door1 => door1.СобытиеРазблокировки(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открывается, door1 => door1.СобытиеБлокировкиЗамка(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Открывается, door1 => door1.ЗаблокироватьЗамок(), false);

        yield return (StateDoor.ДверьОткрыта, StateLock.Закрывается, door1 => door1.Разблокировать("1234"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрывается, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрывается, door1 => door1.ОткрытьДверь(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрывается, door1 => door1.ЗакрытьДверь(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрывается, door1 => door1.СобытиеРазблокировки(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрывается, door1 => door1.СобытиеБлокировкиЗамка(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрывается, door1 => door1.ЗаблокироватьЗамок(), false);

        yield return (StateDoor.ДверьОткрыта, StateLock.Закрыт, door1 => door1.Разблокировать("1234"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрыт, door1 => door1.Разблокировать("123"), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрыт, door1 => door1.ОткрытьДверь(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрыт, door1 => door1.ЗакрытьДверь(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрыт, door1 => door1.СобытиеРазблокировки(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрыт, door1 => door1.СобытиеБлокировкиЗамка(), false);
        yield return (StateDoor.ДверьОткрыта, StateLock.Закрыт, door1 => door1.ЗаблокироватьЗамок(), false);
    }

    public string CompileMessageNotFoundHandler(string trigger, string state, string entity)
    {
        return $"Не найдено ни одного обработчика для триггера {trigger} в состоянии {state} у {entity}";
    }
}