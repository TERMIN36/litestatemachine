﻿using Termin36.LiteStateMachine;

namespace Termin36.LiteStateMachineTests.Samples.Agregates.Door;

public class DoorLock
{
    #region ctor

    public DoorLock(State state, string pingCode)
    {
        CurrentState = state;
        PinCode = pingCode;
        _stateMachine = new StateMachine<State, Trigger>(CurrentState, "DoorLock",
            stateUpdateHandle: state1 => CurrentState = state1);
        Config().Wait();
    }

    #endregion

    #region ConfigureStateMachine

    public async Task Config()
    {
        _stateMachine.Configuration(State.Открыт)
            .ChangeState(Закрываем, State.Закрывается);

        _stateMachine.Configuration(State.Открывается)
            .ChangeState(СобытиеДляОткрытия, State.Открыт);

        _stateMachine.Configuration(State.Закрыт)
            .ChangeStateIf(Открываем, State.Открывается, ПроверитьПинКод);

        _stateMachine.Configuration(State.Закрывается)
            .ChangeState(СобытиеДляЗакрытия, State.Закрыт);

        _stateMachine.ValidateStateMachine();
    }

    #endregion

    #region props

    public string PinCode { get; private set; }

    public State CurrentState { get; set; }
    private readonly StateMachine<State, Trigger> _stateMachine;

    #endregion

    #region State and Triggers

    public enum State
    {
        Открыт,
        Открывается,
        Закрыт,
        Закрывается
    }

    public enum Trigger
    {
        Открываем,
        Закрываем,
        СобытиеДляОткрытия,
        СобытиеДляЗакрытия
    }


    private readonly TriggerWithParameter<Trigger, string> Открываем = new(Trigger.Открываем);
    private readonly TriggerWithParameter<Trigger, string> Закрываем = new(Trigger.Закрываем);
    private readonly TriggerWithParameter<Trigger, object?> СобытиеДляОткрытия = new(Trigger.СобытиеДляОткрытия);
    private readonly TriggerWithParameter<Trigger, object?> СобытиеДляЗакрытия = new(Trigger.СобытиеДляЗакрытия);

    #endregion

    #region PublicMethods

    #region Checks

    public async Task<(bool result, string? message)> ПроверитьВозможностьРазблокировки(object? pinCode)
    {
        var res = await _stateMachine.CanDo(Открываем, pinCode as string);
        return (res.result, res.message?.FirstOrDefault()?.Guards?.FirstOrDefault());
    }

    public async Task<(bool result, string? message)> ПроверитьОткрытЛиЗамок(object? arg)
    {
        return (CurrentState == State.Открыт, "Замок не разблокирован");
    }

    #endregion

    public async Task<(bool result, string? message)> ПроверитьПинКод(string? pinCode)
    {
        return (PinCode.Equals(pinCode), "Неверный пин код");
    }

    public async Task Заблокировать(object? arg)
    {
        await _stateMachine.Do(Закрываем, null);
    }

    public async Task Разблокировать(string? pinCode)
    {
        await _stateMachine.Do(Открываем, pinCode);
    }

    public async Task СобытиеБлокировки(object? arg)
    {
        await _stateMachine.Do(СобытиеДляЗакрытия, null);
    }

    public async Task СобытиеРазблокировки(object? arg)
    {
        await _stateMachine.Do(СобытиеДляОткрытия, null);
    }

    #endregion
}