# О примере

Данный пример показывает один из вариантов внедрения SM в домен. Пример очень абстрактный, не учитывает много
требований к качеству реализации в промышленных системах, однако позволяет понять основную концепцию без лишних деталей.

Большинство переменных и объектов названы русским текстом, это сделано для упрощения восприятия сценария тестирования