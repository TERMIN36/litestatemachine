﻿using Termin36.LiteStateMachine;

namespace Termin36.LiteStateMachineTests.Samples.Agregates.Door;

public class DoorStub : Door
{
    public DoorStub(State state, DoorLock.State stateLock, string pinCode) : base(state, stateLock, pinCode)
    {
    }

    public void SetState(State state, DoorLock stateLock)
    {
        CurrentState = state;
        Замок = stateLock;

        _stateMachine =
            new StateMachine<State, Trigger>(CurrentState, "Door", stateUpdateHandle: state1 => CurrentState = state1);

        Config().Wait();
    }
}