namespace Termin36.LiteStateMachineTests.Samples.Agregates.Door;

public interface IDoor
{
    Task ОткрытьДверь();
    Task Разблокировать(string pinCode);
    Task ЗакрытьДверь();
    Task ЗаблокироватьЗамок();
    Task СобытиеБлокировкиЗамка();
    Task СобытиеРазблокировки();
}