using FluentAssertions;
using Termin36.LiteStateMachine;
using Termin36.LiteStateMachine.Exceptions;

namespace Termin36.LiteStateMachineTests;

public class Tests
{
    public enum State
    {
        StateA,
        StateB,
        StateC
    }

    public enum Trigger
    {
        TriggerA,
        TriggerB
    }

    public TriggerWithParameter<Trigger, string> _triggerA = new(Trigger.TriggerA);
    public TriggerWithParameter<Trigger, string> _triggerB = new(Trigger.TriggerB);

    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public async Task ChangeStateTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeState(_triggerA, State.StateB);

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        var canDo = await stateMachine.CanDo(_triggerA, null);
        canDo.result.Should().BeTrue();
        canDo.message.Should().BeNull();
        await stateMachine.Do(_triggerA, null);
    }

    [Test]
    public async Task ChangeStateIfTest_true()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeStateIf(_triggerA, State.StateB, async x => (true, null), "Guard Description");

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        var canDo = await stateMachine.CanDo(_triggerA, null);
        canDo.result.Should().BeTrue();
        canDo.message.Should().BeNull();

        await stateMachine.Do(_triggerA, null);
        stateMachine.State.Should().Be(State.StateB);
    }

    [Test]
    public async Task ChangeStateIfTest_false()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeStateIf(_triggerA, State.StateB, async x => (false, "DynamicGuard"), "CommonGuard");

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        var canDo = await stateMachine.CanDo(_triggerA, null);
        canDo.result.Should().BeFalse();
        canDo.message.Should().NotBeNull();
        canDo.message!.SelectMany(x => x.Guards!).Should().BeEquivalentTo("DynamicGuard", "CommonGuard");

        var act = async () => await stateMachine.Do(_triggerA, null);
        await act.Should().ThrowAsync<BehaviorException>();
    }

    [Test]
    public async Task ChangeStateDynamicIfTest_true()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeStateDynamicIf(_triggerA, (_, _) => State.StateB, async x => (true, null), "Guard Description");

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        var canDo = await stateMachine.CanDo(_triggerA, null);
        canDo.result.Should().BeTrue();
        canDo.message.Should().BeNull();

        await stateMachine.Do(_triggerA, null);
        stateMachine.State.Should().Be(State.StateB);
    }

    [Test]
    public async Task ChangeStateDynamicIfTest_false()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeStateDynamicIf(_triggerA, (_, _) => State.StateB, async x => (false, null), "Guard Description")
            .ChangeStateDynamicIfInverted(_triggerA, (_, _) => State.StateC, async x => (true, null),
                "Guard Description");

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        var canDo = await stateMachine.CanDo(_triggerA, null);
        canDo.result.Should().BeFalse();
        canDo.message.Should().NotBeNull();
        canDo.message!.SelectMany(x => x.Guards!).Should().BeEquivalentTo("Guard Description");

        var act = async () => await stateMachine.Do(_triggerA, null);
        await act.Should().ThrowAsync<BehaviorException>();
        stateMachine.State.Should().Be(State.StateA);
    }

    [Test]
    public async Task ChangeStateDynamicTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeStateDynamic(_triggerA, (_, _) => State.StateB);

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        var canDo = await stateMachine.CanDo(_triggerA, null);
        canDo.result.Should().BeTrue();
        canDo.message.Should().BeNull();

        await stateMachine.Do(_triggerA, "asd");
        stateMachine.State.Should().Be(State.StateB);
    }

    [Test]
    public async Task OnEntryAndOnExitTest()
    {
        List<string> handled = new();
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .OnEntry(async x => handled.Add("OnEntryStateA"))
            .ChangeState(_triggerA, State.StateB)
            .ChangeState(_triggerB, State.StateB)
            .OnExitFrom(_triggerA.Trigger, async x => handled.Add("OnExitStateAFromTriggerA"))
            .OnExit(async x => handled.Add("OnExitStateA"));

        stateMachine.Configuration(State.StateB)
            .OnEntry(async x => handled.Add("OnEntryStateB"))
            .OnEntryFrom(_triggerA.Trigger, async x => handled.Add("OnEntryStateBFromTriggerA"))
            .OnExit(async x => handled.Add("OnExitStateB"));
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        await stateMachine.Do(_triggerA, "test");

        handled.Should().BeEquivalentTo(new List<string>
            {"OnExitStateA", "OnExitStateAFromTriggerA", "OnEntryStateB", "OnEntryStateBFromTriggerA"});
    }

    [Test]
    public async Task NotChangeStateTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .NotChangeState(_triggerA);

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();
        var res = await stateMachine.CanDo(_triggerA, null);
        res.result.Should().BeTrue();
        await stateMachine.Do(_triggerA, null);
        stateMachine.State.Should().Be(State.StateA);
    }

    [Test]
    public async Task NotChangeStateIfTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .NotChangeStateIf(_triggerA, async x => (true, null), "Guard Description");

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();
        var res = await stateMachine.CanDo(_triggerA, null);
        res.result.Should().BeTrue();
        await stateMachine.Do(_triggerA, null);
        stateMachine.State.Should().Be(State.StateA);
    }

    [Test]
    public async Task NotTriggerTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA);
        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();
        var res = await stateMachine.CanDo(_triggerA, null);
        res.result.Should().BeFalse();
        res.message.Should().NotBeNull();
        res.message.First().FullMessage.Should().StartWith("Не найдено ни одного обработчика для триггера");
        var act = async () => await stateMachine.Do(_triggerA, null);
        await act.Should().ThrowAsync<BehaviorException>();
        stateMachine.State.Should().Be(State.StateA);
    }

    [Test]
    public async Task DoubleBehaviorTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeState(_triggerA, State.StateB)
            .ChangeState(_triggerA, State.StateB);
        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();
        var res = await stateMachine.CanDo(_triggerA, null);
        res.result.Should().BeFalse();
        res.message.Should().NotBeNull();
        res.message.First().FullMessage.Should()
            .StartWith($"У триггера {_triggerA.Trigger} обнаружено более одного срабатывающего обработчика типа");
        var act = async () => await stateMachine.Do(_triggerA, null);
        await act.Should().ThrowAsync<BehaviorException>();
        stateMachine.State.Should().Be(State.StateA);
    }

    [Test]
    public async Task DoubleConfigurationTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");
        stateMachine.Configuration(State.StateA);
        stateMachine.Configuration(State.StateA);
        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);
        stateMachine.ValidateStateMachine();
    }

    [Test]
    public async Task OnActivateAndOnDeactivateTest()
    {
        List<string> handled = new();
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .OnActivate(async () => handled.Add("OnActivateA"))
            .ChangeState(_triggerA, State.StateB)
            .OnDeactivate(async () => handled.Add("OnDeactivateA"));

        stateMachine.Configuration(State.StateB)
            .OnActivate(async () => handled.Add("OnActivateB"))
            .OnDeactivate(async () => handled.Add("OnDeactivateB"));
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        await stateMachine.Activate();
        await stateMachine.Do(_triggerA, null);
        await stateMachine.Deactivate();

        handled.Should().BeEquivalentTo(new List<string> {"OnActivateA", "OnDeactivateB"});
    }

    [Test]
    public async Task OnActionTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .OnAction(_triggerA, async _ => { });

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        await stateMachine.Do(_triggerA, "test");
    }

    [Test]
    public async Task OnActionIfTest()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .OnActionIf(_triggerA, async _ => { }, async x => (true, null), "Guard Description");

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        await stateMachine.Do(_triggerA, "test");
    }

    [Test]
    public async Task OnTransition()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine");

        stateMachine.Configuration(State.StateA)
            .ChangeState(_triggerA, State.StateB);

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        Transaction<State, Trigger>? transition = null;
        stateMachine.OnTransition += t => transition = t;

        stateMachine.ValidateStateMachine();

        await stateMachine.Do(_triggerA, "test");

        transition.Should().NotBeNull();
        transition.Trigger.Should().Be(Trigger.TriggerA);
    }

    [Test]
    public async Task OnFailHandler()
    {
        var stateMachine = new StateMachine<State, Trigger>(State.StateA, "StateMachine", onFailHandler: OnFailHandler);

        stateMachine.Configuration(State.StateA)
            .ChangeState(_triggerA, State.StateB);

        stateMachine.Configuration(State.StateB);
        stateMachine.Configuration(State.StateC);

        stateMachine.ValidateStateMachine();

        var act = async () => await stateMachine.Do(_triggerB, "test");
        var throwAsync = await act.Should().ThrowAsync<BehaviorException>();
        throwAsync.And.Message.Should().Be("CustomErrorForStateA");

        BehaviorException OnFailHandler(State state, Trigger trigger, object? arg, Type? argType,
            List<GuardDescription>? messages)
        {
            switch (state)
            {
                case State.StateA:
                    return new BehaviorException("CustomErrorForStateA", messages.First());
                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }
    }
}