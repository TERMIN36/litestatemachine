# О проекте

Проект формирует библиотеку для построения машин конечных состояний (State Machine), с помощью которых возможно строить
сложную модель поведения с реакцией на воздействия. Ценность библиотеки в простоте и независимости от окружения,
благодаря чему ее
можно без сомнений интегрировать например в слой домена (DDD)

Более подробно можно ознакомиться в [Wiki](https://gitlab.com/TERMIN36/litestatemachine/-/wikis/home)
По вопросам и развитию ходить [сюда](https://gitlab.com/TERMIN36/litestatemachine/-/issues)

# Возможности ядра

- Хранение состояния во внешнем хранилище
- Журнал транзакций
- Обработка событий
    - Активация и деактивация состояния (OnActivate и OnDeactivate)
    - Вход и выход из состояния (OnEntry и OnExit)
    - Вход и выход из состояния с фильтром по триггеру (OnEntryFrom и OnExitFrom)
    - Обработка пользовательского события на срабатывание триггера (OnAction, OnActionIf)
- Переходы между состояниями
    - Без проверки условий
        - Переход с известным конечным состоянием (ChangeState)
        - Переход с определением конечного состояния с помощью функции (ChangeStateDynamic)
        - Игнорирование перехода (NotChangeState)
    - С проверкой условия
        - Переход с известным конечным состоянием с проверкой условия (ChangeStateIf)
        - Переход с определением конечного состояния с помощью функции с проверкой условия (ChangeStateDynamicIf)
        - Игнорирование перехода с условием (NotChangeStateIf)

# Пример использования

Устанавливаем пакет

```
dotnet add package Termin36.LiteStateMachine --version x.x.x
```

Используем

```csharp
StateMachine<State, Trigger> stateMachine = new StateMachine<State, Trigger>(State.StateA);

stateMachine.Configuration(State.StateA)
    .OnAction(_triggerRegister, RegistrationPeople);
    .ChangeState(_triggerMove, State.StateMoving);

stateMachine.Configuration(State.StateMoving)
    .ChangeStateIf(_triggerRegister, State.StateA, IsAllowRegistration, "Регистрировать пользователей сейчас нельзя");

stateMachine.ValidateStateMachine();

await stateMachine.Fire(_triggerRegister, new People("Иванов", "Иван");
await stateMachine.Fire(_triggerMove, null);
await stateMachine.Fire(_triggerRegister, new People("Петров", "Петр"); // сработает исключение, так как обработать нельзя
```

Более подробно можно ознакомиться в [Wiki](https://gitlab.com/TERMIN36/litestatemachine/-/wikis/home)
